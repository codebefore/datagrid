import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseData, City, RequestData } from './model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
        responseData:ResponseData= new ResponseData();
        cities:City[] = new Array<City>();
  constructor(private http:HttpClient) { }

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }

  getCities(request:RequestData){
    console.log(request);
    return this.http.post('https://friend.codebefore.com/api/cities',request,{
       headers:this.createRequestOptions()
     });
  }
}
