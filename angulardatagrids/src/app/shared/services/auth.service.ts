import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
// import decode from 'jwt-decode';

@Injectable()
export class AuthService {
  loggedIn = false;

  constructor(private router: Router, public cookie: CookieService) { }

  logIn(login: string, password: string) {
    this.cookie.set("login","true")
    console.log(this.isLoggedIn, "login");
    this.loggedIn = true;
    this.router.navigate(['main']);
  }

  logOut() {
    this.cookie.deleteAll();
    this.loggedIn = false;
    console.log(this.isLoggedIn, "logout");
    this.router.navigate(['/']);
  }

  get isLoggedIn() {
    if (this.cookie.get("login") === "true") {
      this.loggedIn = true;
    }
    else {
      this.loggedIn = false;
    }
    return this.loggedIn;
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = this.authService.isLoggedIn;
    const isLoginForm = route.routeConfig.path === '/';

    if (isLoggedIn && isLoginForm) {
      this.router.navigate(['/definition']);
      // return false;
    }

    if (!isLoggedIn && !isLoginForm) {
      this.router.navigate(['/']);
    }

    return isLoggedIn || isLoginForm;
  }
}
