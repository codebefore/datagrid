import { RequestType } from './requestType.enum';
import { Request } from './request';
import { User } from '../modules/definition/service';
export class RequestData {
    request:Request= new Request();
    data:any;
    requestType:RequestType;
    user:User;
    
}
