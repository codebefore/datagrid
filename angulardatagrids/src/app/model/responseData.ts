import { User } from '../modules/definition/service';

export class ResponseData {
    id:string;
    isSucceed:boolean=false;
    message:string;
    data:any;
    count:number;
    user:User
}
