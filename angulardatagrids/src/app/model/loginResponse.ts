import { User } from '../modules/definition/service/user';

export class LoginResponse {
    accessToken:string;
    user:User;
    isSucceed:boolean;
}
