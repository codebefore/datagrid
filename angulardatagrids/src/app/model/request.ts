export class Request {
  skip: number;
  take: number;
  id: number;
  filters:FilterObject[]= new Array<FilterObject>();
  includes:string[]=new Array<string>();
}

export class FilterObject {
  column: string;
  condition: string;
  value: string;
}

// export class IncludeObject {
//   include: string;
//   thenInclude: string;
//   then: boolean=false;
// }
