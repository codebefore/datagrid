export enum RequestType {
    Create = 1,
    Read = 2,
    Update = 3,
    Delete = 4,
    List = 5
}
