import { Component, OnInit, NgModule, Input, ChangeDetectorRef } from "@angular/core";
import {
  SideNavigationMenuModule,
  HeaderModule
} from "../../shared/components";
import { ScreenService, AuthService } from "../../shared/services";
import { DxDrawerModule } from "devextreme-angular/ui/drawer";
import { DxScrollViewModule } from "devextreme-angular/ui/scroll-view";
import { CommonModule } from "@angular/common";

import { navigation } from "../../app-navigation";
import { Router, NavigationEnd, RouterModule } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-side-nav-outer-toolbar",
  templateUrl: "./side-nav-outer-toolbar.component.html",
  styleUrls: ["./side-nav-outer-toolbar.component.scss"]
})
export class SideNavOuterToolbarComponent implements OnInit {
  menuItems;
  selectedRoute = "";
  public roleIds: string[] = new Array<string>();

  menuOpened: boolean;
  temporaryMenuOpened = false;

  @Input()
  title: string;

  menuMode = "shrink";
  menuRevealMode = "expand";
  minMenuSize = 0;
  shaderEnabled = false;
  username: string;
  constructor(
    private screen: ScreenService,
    private router: Router,
    public cookie: CookieService,
    public authService: AuthService,
    private ref: ChangeDetectorRef
  ) {

    this.roleIds = JSON.parse(this.cookie.get("roles"));
    this.username = this.cookie.get("name");

    if (this.roleIds.indexOf("3") !== -1) {
      this.menuItems = navigation;
    }
    else if (this.roleIds.indexOf("4") !== -1) {
      for (let index = 0; index < navigation.length; index++) {
        if (navigation[index].id === 2) {
          this.menuItems = navigation.splice(index, 1);
        }
      }
    }


    console.log(this.menuItems);

  }
  logOut = () => {
    this.cookie.deleteAll("/");
    this.authService.logOut();
  }

  ngOnInit() {
    this.menuOpened = this.screen.sizes["screen-large"];

    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.selectedRoute = val.urlAfterRedirects.split("?")[0];
      }
    });

    this.screen.changed.subscribe(() => this.updateDrawer());

    this.updateDrawer();
  }

  updateDrawer() {
    const isXSmall = this.screen.sizes["screen-x-small"];
    const isLarge = this.screen.sizes["screen-large"];

    this.menuMode = isLarge ? "shrink" : "overlap";
    this.menuRevealMode = isXSmall ? "slide" : "expand";
    this.minMenuSize = isXSmall ? 0 : 60;
    this.shaderEnabled = !isLarge;
  }

  get hideMenuAfterNavigation() {
    return this.menuMode === "overlap" || this.temporaryMenuOpened;
  }

  get showMenuAfterClick() {
    return !this.menuOpened;
  }

  navigationChanged(event) {
    const path = event.itemData.path;
    const pointerEvent = event.event;

    if (path && this.menuOpened) {
      if (event.node.selected) {
        pointerEvent.preventDefault();
      } else {
        this.router.navigate([path]);
      }

      if (this.hideMenuAfterNavigation) {
        this.temporaryMenuOpened = false;
        this.menuOpened = false;
        pointerEvent.stopPropagation();
      }
    } else {
      pointerEvent.preventDefault();
    }
  }

  navigationClick() {
    if (this.showMenuAfterClick) {
      this.temporaryMenuOpened = true;
      this.menuOpened = true;
    }
  }
}

@NgModule({
  imports: [
    SideNavigationMenuModule,
    DxDrawerModule,
    HeaderModule,
    DxScrollViewModule,
    CommonModule, RouterModule
  ],
  exports: [SideNavOuterToolbarComponent],
  declarations: [SideNavOuterToolbarComponent]
})
export class SideNavOuterToolbarModule { }
