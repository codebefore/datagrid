import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './shared/services';
import { ProfileComponent } from './pages/profile/profile.component';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

const routes: Routes = [
 
  {
    path: '',
    loadChildren:'./modules/login/login.module#LoginModule'
  },
  {
    path: '%23',
    loadChildren:'./modules/main/main-route.module#MainRouteModule'
  },
  {
    path: 'main',
    loadChildren:'./modules/main/main-route.module#MainRouteModule'
  },
  {
    path: 'definition',
    loadChildren:'./modules/definition/definition-route.module#DefinitionRouteModule'
  },
  {
    path: 'problem',
    loadChildren:'./modules/problem/problem-route.module#ProblemRouteModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), DxDataGridModule, DxFormModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [ProfileComponent]
})
export class AppRoutingModule { }
