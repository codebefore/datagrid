import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { AppRoutingModule } from './app-routing.module';
import { TablegridComponent } from './pages/tablegrid/tablegrid.component';
import { DxDataGridModule, DxTreeViewModule } from 'devextreme-angular';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import it to change locale and load localization messages
import { locale, loadMessages } from 'devextreme/localization';
declare var require: any;
const trMessages = require('devextreme/localization/messages/tr.json');
// import trMessages from 'devextreme/localization/messages/tr.json';
loadMessages(trMessages);
locale('tr');

@NgModule({
  declarations: [
    AppComponent,
    TablegridComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,BrowserAnimationsModule ,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    AppRoutingModule,
    DxDataGridModule,
    HttpClientModule
  ],
  providers: [AuthService, ScreenService, AppInfoService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
