export const navigation = [
  {
    id: 0,
    text: 'Main',
    icon: 'dx-icon dx-icon-home',
    items: [{
      text: 'Grafik',
      path: '/main/graph'
    },]
  },
  {
    id: 2,
    text: 'Uygunsuzluk',
    icon: 'dx-icon dx-icon-notequal',
    items: [
      {
        text: 'Uygunsuzluk Bildir',
        path: '/problem'
      },
      {
        text: 'Uygunsuzluk Listesi',
        path: '/problem/list'
      }
    ]
  }
  ,
  {
    id: 1,
    text: 'Ayarlar',
    icon: 'dx-icon dx-icon-hierarchy',
    items: [
      {
        text: 'Kullanıcı Grubu',
        path: '/definition/user-type'
      },
      {
        text: 'Kullanıcı',
        path: '/definition/'
      }
      // ,
      // {
      //   text: 'Roller',
      //   path: '/definition/role'
      // }
      ,
      {
        text: 'Uygunsuzluk Turu',
        path: '/definition/action-type'
      }
      ,
      {
        text: 'Fabrika',
        path: '/definition/factory'
      },
      {
        text: 'Departman',
        path: '/definition/department'
      },
      {
        text: 'Atolye',
        path: '/definition/atolye'
      },
      {
        text: 'Kisim',
        path: '/definition/kisim'
      },
      {
        text: 'Cihaz Grubu',
        path: '/definition/device-group'
      },
      {
        text: 'Cihaz',
        path: '/definition/device'
      }
    ]
  }
];
