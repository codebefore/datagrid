import { Injectable } from "@angular/core";
import {  RequestData } from "src/app/model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: "root"
})
export class ProblemService {
  url:string='https://prometal.azurewebsites.net/';
  // private url: string = "http://localhost:5001/";
  private actionUrlParameter: string = "api/actions";
  private actionDetailUrlParameter: string = "api/actionDetails";
  private actionAssignUrlParameter: string = "api/actionAssigns";
  constructor(private http: HttpClient, private cookie: CookieService) {}

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.cookie.get("token"),
      "Access-Control-Allow-Origin": "*"
    });
    return headers;
  }

  token(request: RequestData) {
    return this.http.post(this.url + "token", request.data, {
      headers: this.createRequestOptions()
    });
  }

  ActionCrud(request: RequestData) {
    return this.http.post(this.url + this.actionUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }
  ActionAssignCrud(request: RequestData) {
    return this.http.post(this.url + this.actionAssignUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }
  ActionDetailCrud(request: RequestData) {
    return this.http.post(this.url + this.actionDetailUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }


}
