export class ActionStatus {
  id: number;
  name: string;

  getStatus() {
    return [
      {
        id: 0,
        name: "Waiting .."
      },
      {
        id: 1,
        name: "Closed"
      },
      {
        id: 2,
        name: "InProgress .."
      },
    ];
  }
}
