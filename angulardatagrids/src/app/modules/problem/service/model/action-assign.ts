import { Action } from "./action";
import { ActionDetail } from './action-detail';

export class ActionAssign {
  id:number;
  actionId: number;
  selfUserId: number;
  assignUserId: number=0;
  status: number;
  action: Action = new Action();
  actionDetails: ActionDetail[] = new Array<ActionDetail>();

  createdDate:string;
  selfName:string;
  assignName:string;
}
