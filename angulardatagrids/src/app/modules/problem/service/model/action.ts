import { ActionMedia } from "./actionMedia";
import { Asset } from "src/app/modules/definition/service";
import { ActionDetail } from "./action-detail";
import { ActionAssign } from './action-assign';

export class Action {
  // entity properties
  id: number;
  actionCategoryId: number;
  actionTypeId: number;
  assetId: number;
  status: number;
  no: string = "";
  name: string = "";
  description: string = "";

  //dto properties
  assetName: string;
  statusName: string;
  actionCategoryName: string;
  resUserId: number = 0;
  createdUserId: number = 0;

  
  actionMedias: ActionMedia[] = new Array<ActionMedia>();
  asset: Asset = new Asset();
  actionDetails: ActionDetail[] = new Array<ActionDetail>();
  actionAssigns: ActionAssign[] = new Array<ActionAssign>();
}
