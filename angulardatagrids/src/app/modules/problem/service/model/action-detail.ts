import { Action } from './action';
import { ActionAssign } from './action-assign';

export class ActionDetail {
    id:number;
    actionId: number;
    actionAssignId: number;
    name:string="";
    description:string="";

    action:Action=new Action();
    actionAssign:ActionAssign=new ActionAssign();
}
