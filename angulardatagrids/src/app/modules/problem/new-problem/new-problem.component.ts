import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  enableProdMode,
  ChangeDetectorRef
} from "@angular/core";
import { BarcodeFormat } from "@zxing/library";
import { BehaviorSubject, Subject, Observable } from "rxjs";
import { FormatsDialogComponent } from "../componentsOfProblem/formats-dialog-component/formats-dialog.component";
import { AppInfoDialogComponent } from "../componentsOfProblem/app-info-dialog-component/app-info-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { DefinitionService, User, Item, ActionType } from "../../definition/service";
import { HttpClient } from "@angular/common/http";
import {
  ResponseData,
  RequestData,
  RequestType,
  FilterObject
} from "src/app/model";
import { Asset } from "../../definition/service/asset";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { ProblemService } from "../service";
import { Action, ActionMedia } from "../service/model";
import { WebcamInitError, WebcamImage, WebcamUtil } from "ngx-webcam";
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { DomSanitizer } from "@angular/platform-browser";

// if (!/localhost/.test(document.location.host)) {
//   enableProdMode();
// }

@Component({
  selector: "app-new-problem",
  templateUrl: "./new-problem.component.html",
  styleUrls: ["./new-problem.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewProblemComponent implements OnInit {
  faGlobe = faGlobe;
  public Editor = ClassicEditor;
  public qrStep = false;
  public mainStep = false;
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
    WebcamUtil.getAvailableVideoInputs().then(
      (mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      }
    );
  }
  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo = null;

  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE
  ];

  hasDevices: boolean;
  hasPermission: boolean;

  qrResultString: string;

  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;

  //#region webcam
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    // width: {ideal: 1024},
    // height: {ideal: 576}
  };
  public errors: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage: WebcamImage = null;

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<
    boolean | string
  >();
  //#endregion

  responseData: ResponseData;
  request: RequestData = new RequestData();
  asset: Asset = new Asset();
  assets: Asset[] = new Array<Asset>();
  types: ActionType[] = new Array<ActionType>();
  type: ActionType = new ActionType();
  items: Item[] = new Array<Item>();
  action: Action = new Action();
  actionMedia: ActionMedia;
  users: User[] = new Array<User>();
  user: User = new User();
  message: string;
  success: number = 0;
  images: WebcamImage[] = new Array<WebcamImage>();
  filter: FilterObject = new FilterObject();
  assetSelected:boolean=false;
  webcamContainer: boolean = false;
  problemForm: boolean = false;
  scannerStatus: boolean = true;

  products: any;
  prossesing = false;
  title:string="Uygunsuzluk Bildir"
  constructor(
    private readonly _dialog: MatDialog,
    private definitionService: DefinitionService,
    private problemService: ProblemService,
    public http: HttpClient,
    private ref: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
  ) {
    this.getUsers();
    this.getActionTypes();
    this.getItems();

    // this.products = this.problemService.getProducts();
  }

  setProblem() {
    console.log(this.asset, "select asset");

    this.prossesing = true;
    this.action.assetId = this.asset.id;
    this.action.actionCategoryId = 2;
    this.action.actionTypeId = this.type.id;
    this.request.data = this.action;
    if (this.images.length > 0) {
      this.actionMedia = new ActionMedia();
      this.images.forEach(image => {
        this.actionMedia.url = image.imageAsBase64;
        this.action.actionMedias.push(this.actionMedia);
      });
    }
    this.request.requestType = RequestType.Create;
    this.problemService.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) {
          console.log(this.responseData);
          this.message =
            "Success, " +
            this.responseData.user.name +
            "  adli kisiye gonderilmistir.";
          this.success = 1;
        } else {
          this.message = "Failed";
          this.success = 2;
        }
        this.prossesing = false;

        this.ref.detectChanges();
      },
      err => {
        this.message = err;
      }
    );
  }
  backtoSelectAsset = () => {
    this.mainStep = false;
    this.qrStep = false;
    this.webcamContainer = false;
    this.scannerStatus = true;
    this.asset.name="";
    this.title="Uygunsuzluk Bildir.";
    this.assetSelected=false;
    this.ref.detectChanges();
  }

  getQr = () => {
    this.mainStep = true;
    this.qrStep = true;
    if(this.assetSelected)
       this.title=`${this.asset.name} 'da bulunan problemin resmini cekin ya da devam edin`;
    else this.title="QrCode veya Barcode okutunuz.";
    this.ref.detectChanges();

  };
  
  findAsset=(assetId: number)=> {

    console.log("findasset");
    this.request=new RequestData();
    this.request.requestType = RequestType.Read;

    this.filter.column = "id";
    this.filter.condition = "==";
    this.filter.value = assetId.toString();
    this.request.request.filters.push(this.filter);
    this.definitionService.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        
        
        if (this.responseData.isSucceed) {
          this.asset = <Asset>this.responseData.data;
          console.log(this.responseData, "asset");
          this.action.resUserId=this.asset.resUserId;
          this.findUser(this.action.resUserId);
          this.scannerStatus = false;
          this.webcamContainer = true;
          this.getQr();
          this.ref.detectChanges();
          // camerayi kapat
        }
      },
      err => {
        console.log(err);
      }
    );
  }
  findUser(userId: number) {
    for (let index = 0; index < this.users.length; index++) {
        if(this.users[index].id===userId)
         this.user = this.users[index];
    }

  }
  getUsers() {
    this.request.requestType = RequestType.List;
    this.definitionService.UserCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) {
          this.users = <Array<User>>this.responseData.data;
          // camerayi kapat
        }
        this.ref.detectChanges();
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }

  getActionTypes=()=> {
    this.request.requestType = RequestType.List;
    this.definitionService.ActionTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) {
          this.types = <Array<ActionType>>this.responseData.data;
          // camerayi kapat
        }
        this.ref.detectChanges();
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }

  selectAsset = event => {
    this.assetSelected=true;
    // this.webcamContainer = true;
    this.title="Kamera'nin ustune tiklalyarak resim çekebilir ya da devam edebilirsiniz.";
    this.scannerStatus = true;
    this.asset.id = event.itemData.id;
    if (this.asset.id > 0 && this.asset.name.length == 0)
      this.findAsset(this.asset.id);
      this.ref.detectChanges();

  };
  getItems() {
    this.request.requestType = RequestType.List;
    this.asset = new Asset();
    this.asset.isRecursive = true;
    this.request.data = this.asset;
    this.definitionService.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) {
          this.items = <Array<Item>>this.responseData.data;
        }
        this.ref.detectChanges();
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }

  clearResult(): void {
    this.qrResultString = null;
  }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    console.log(this.qrResultString, "qr result");

    this.asset.id = parseInt(this.qrResultString);
    if (this.asset.id > 0 && this.asset.name.length == 0)
      this.findAsset(this.asset.id);
  }

  onDeviceSelectChange(selected: string) {
    const device = this.availableDevices.find(x => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  openFormatsDialog() {
    const data = {
      formatsEnabled: this.formatsEnabled
    };

    this._dialog
      .open(FormatsDialogComponent, { data })
      .afterClosed()
      .subscribe(x => {
        if (x) {
          this.formatsEnabled = x;
        }
      });
  }

  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  openInfoDialog() {
    const data = {
      hasDevices: this.hasDevices,
      hasPermission: this.hasPermission
    };

    this._dialog.open(AppInfoDialogComponent, { data });
  }

  onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  toggleTryHarder(): void {
    this.tryHarder = !this.tryHarder;
  }

  //#region  webcam
  public triggerSnapshot(): void {
    this.trigger.next();
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.log(webcamImage, "webcam iamges");

    this.images.push(webcamImage);
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log("active device: " + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  public removeImage(index: number) {
    this.images.splice(index, 1);
  }
  //#endregion

  gotoFromStep() {
    this.title="Yeni Uygunsuzluk";
    this.problemForm = true;
    this.webcamContainer = false;
  }
}
