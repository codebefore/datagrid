import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ProblemService } from "../service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {
  RequestData,
  RequestType,
  ResponseData,
  FilterObject
} from "src/app/model";
import {
  Action,
  ActionStatus,
  ActionDetail,
  ActionAssign
} from "../service/model";
import { User, DefinitionService, Asset, ActionType } from "../../definition/service";
// import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { CookieService } from 'ngx-cookie-service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: "app-problem-list",
  templateUrl: "./problem-list.component.html",
  styleUrls: ["./problem-list.component.scss"]
})
export class ProblemListComponent implements OnInit {
  popupVisible = false;
  popupHistory = false;
  public Editor = ClassicEditor;
  dataSource: any = {};
  itemlist: Action[];
  users: User[];
  assets: Asset[];
  item: Action = new Action();
  actionDetail: ActionDetail = new ActionDetail();
  responseData: ResponseData;
  responseUsers: ResponseData;
  responseAssets: ResponseData;
  responseCreateDetail: ResponseData;
  responseCreateAssign: ResponseData;
  request: RequestData;
  filter: FilterObject = new FilterObject();
  actionStatus: ActionStatus[] = new Array<ActionStatus>();
  actionTypes: ActionType[] = new Array<ActionType>();
  actionStatu: ActionStatus = new ActionStatus();
  actionAssigns: ActionAssign[] = new Array<ActionAssign>();
  actionAssign: ActionAssign = new ActionAssign();
  newactionAssign: ActionAssign = new ActionAssign();
  message: string;
  success: number = 0;
  prossesing = false;
  @ViewChild("longContent", { static: true }) longContent: TemplateRef<any>;
  @ViewChild("historyContent", { static: true }) historyContent: TemplateRef<any>;
  public actionId: number = 0;
  public roleIds: string[] = new Array<string>();
  public adminMode: boolean = true;
  title: string = 'Uygunsuzluk Listesi';
  constructor(
    public service: ProblemService,
    public defService: DefinitionService,
    public http: HttpClient,
    public _DomSanitizationService: DomSanitizer,
    public cookie: CookieService,
    private modalService: NgbModal

  ) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    let vm = this;
    this.getList();
    this.getPie();
    this.getUsers();
    this.getAssets();
    this.getTypes();

    this.actionStatus = <Array<ActionStatus>>this.actionStatu.getStatus();


    this.roleIds = JSON.parse(this.cookie.get("roles"));


    if (this.roleIds.indexOf("3") !== -1) {

    }
    else if (this.roleIds.indexOf("4") !== -1) {
      this.adminMode = false;
    }
  }

  ngOnInit() { }
  insert(e) {
    this.request.requestType = RequestType.Create;
    this.item = <Action>e.data;
    this.item.actionCategoryId = 2;
    this.request.data = e.data;
    this.service.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.getList();
        else console.log(this.responseData, "insert failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e) {
    this.request.requestType = RequestType.Update;
    this.request.data = <Action>e.data;
    this.service.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "udpate ok");
        else console.log(this.responseData, "udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType = RequestType.Delete;
    this.request.data = <Action>e.data;
    this.service.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "delete ok");
        else console.log(this.responseData, "delete failed");
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if (!gridCell) {
      return;
    }
  }

  public getList() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column = "actionCategoryId";
    this.filter.condition = "==";
    this.filter.value = "2";
    this.request.request.filters.push(this.filter);

    // this.filter = new FilterObject();
    // this.filter.column = "status";
    // this.filter.condition = "==";
    // this.filter.value = "0";
    // this.request.request.filters.push(this.filter);
    // this.request.request.includes.push("Asset");
    this.service.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.itemlist = this.responseData.data;
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }

  public getPie() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column = "actionCategoryId";
    this.filter.condition = "==";
    this.filter.value = "2";
    this.request.request.filters.push(this.filter);
    this.defService.GetActionStatisticForPieChart(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }
  public getTypes() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    // this.filter.column = "actionCategoryId";
    // this.filter.condition = "==";
    // this.filter.value = "2";
    // this.request.request.filters.push(this.filter);

    // this.filter = new FilterObject();
    // this.filter.column = "status";
    // this.filter.condition = "==";
    // this.filter.value = "0";
    // this.request.request.filters.push(this.filter);
    // this.request.request.includes.push("Asset");
    this.defService.ActionTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.actionTypes = this.responseData.data;
        console.log(this.responseData);
      },
      err => {
        console.log(err);
      }
    );
  }

  public getUsers() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.defService.UserCrud(this.request).subscribe(
      res => {
        console.log(res, "users ");
        this.responseUsers = <ResponseData>res;
        if (this.responseUsers.isSucceed) this.users = this.responseUsers.data;
      },
      err => {
        console.log(err);
      }
    );
  }

  getAssets = () => {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.defService.AssetCrud(this.request).subscribe(
      res => {
        console.log(res, "assets ");
        this.responseAssets = <ResponseData>res;
        if (this.responseAssets.isSucceed)
          this.assets = this.responseAssets.data;
      },
      err => {
        console.log(err);
      }
    );
  };

  public detay = (data: { row: { data: Action } }) => {
    console.log(data.row.data.id, "content");
    this.item = data.row.data;
    this.getActionDetail(0);
  };

  public historyDetail = (data: { row: { data: Action } }) => {
    console.log(data.row.data, "content");
    this.item = data.row.data;
    this.getActionDetail(1);
  };
  public clear = () => {
    this.actionAssigns = new Array<ActionAssign>();
  }
  public getActionDetail = (index) => {
    this.clear();
    this.request = new RequestData();
    this.request.requestType = RequestType.Read;
    this.filter.column = "id";
    this.filter.condition = "==";
    this.filter.value = this.item.id.toString();
    this.request.request.filters.push(this.filter);
    this.request.request.includes.push("ActionAssigns");
    this.request.request.includes.push("ActionMedias");
    this.request.request.includes.push("ActionDetails");
    this.request.request.includes.push("Asset");
    this.service.ActionCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.item = this.responseData.data;
        console.log(this.item, "action detail");

        for (let index = 0; index < this.item.actionAssigns.length; index++) {
          this.actionAssign = new ActionAssign();
          this.actionAssign.assignName = this.getUserFullName(
            this.item.actionAssigns[index].assignUserId
          );

          this.actionAssign.selfName = this.getUserFullName(
            this.item.actionAssigns[index].selfUserId
          );
          this.actionAssign.id = this.item.actionAssigns[index].id;
          this.actionAssign.createdDate = this.item.actionAssigns[index].createdDate;
          this.actionAssign.actionDetails = this.item.actionAssigns[
            index
          ].actionDetails;
          this.actionAssigns.push(this.actionAssign);
        }

        if (index === 0) {
          this.openDetailModal();
        }
        else {
          this.openHistoryModal();

        }
      },
      err => {
        console.log(err);
      }
    );
  };

  public openDetailModal() {
    this.popupVisible=true;
  }

  public openHistoryModal() {
    this.popupHistory=true;

  }


  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }

  public getUserFullName(id: number): string {
    for (let index = 0; index < this.users.length; index++) {
      if (this.users[index].id == id) {
        return this.users[index].fullName;
      }
    }
  }

  public save = i => {
    this.prossesing = true;
    if (i == 0) {
      this.request = new RequestData();
      this.request.requestType = RequestType.Create;
      this.actionDetail.actionId = this.item.id;
      this.actionDetail.action = null;
      this.actionDetail.actionAssign = null;
      this.actionDetail.actionAssignId = this.actionAssigns[
        this.actionAssigns.length - 1
      ].id;
      this.request.data = this.actionDetail;
      this.service.ActionDetailCrud(this.request).subscribe(
        res => {
          console.log(res, "actiondetail Res ");
          this.responseCreateAssign = <ResponseData>res;
          if (this.responseCreateAssign.isSucceed) {
            this.message = "Success, Kaydedildi.";
            this.success = 1;
          } else {
            this.message = "Failed";
            this.success = 2;
          }

          this.prossesing = false;
        },
        err => {
          console.log(err);

          this.prossesing = false;
        }

      );
    } else if (i == 1) {
      this.request = new RequestData();
      this.request.requestType = RequestType.Create;
      this.actionDetail.actionId = this.item.id;
      this.actionDetail.action = null;
      this.actionDetail.actionAssign = null;
      this.actionAssign = new ActionAssign();
      if (this.newactionAssign.assignUserId > 0)
        this.actionAssign.assignUserId = this.newactionAssign.assignUserId;
      else this.actionAssign.assignUserId = this.item.createdUserId;
      this.actionAssign.actionId = this.item.id;
      this.actionAssign.status = 3;
      if (this.actionDetail.name != "")
        this.actionAssign.actionDetails.push(this.actionDetail);
      this.actionAssign.action = null;
      this.request.data = this.actionAssign;
      this.service.ActionAssignCrud(this.request).subscribe(
        res => {
          console.log(res, "actionassign Res ");
          this.responseCreateDetail = <ResponseData>res;
          if (this.responseCreateDetail.isSucceed) {
            this.message = "Success, Yonlendirildi.";
            this.success = 1;
          } else {
            this.message = "Failed";
            this.success = 2;
          }

          this.prossesing = false;
        },
        err => {
          console.log(err);
          this.prossesing = false;

        }
      );
    }
    else if (i == 2) {
      this.request = new RequestData();
      this.request.requestType = RequestType.Create;
      this.actionDetail.actionId = this.item.id;
      this.actionDetail.action = null;
      this.actionDetail.actionAssign = null;
      this.actionAssign = new ActionAssign();

      if (this.newactionAssign.assignUserId > 0)
        this.actionAssign.assignUserId = this.newactionAssign.assignUserId;
      else this.actionAssign.assignUserId = this.item.createdUserId;
      this.actionAssign.actionId = this.item.id;
      this.actionAssign.status = 4;
      if (this.actionDetail.name != "")
        this.actionAssign.actionDetails.push(this.actionDetail);
      this.actionAssign.action = null;
      this.request.data = this.actionAssign;
      this.service.ActionAssignCrud(this.request).subscribe(
        res => {
          console.log(res, "actionassign Res ");
          this.responseCreateDetail = <ResponseData>res;
          if (this.responseCreateDetail.isSucceed) {
            this.message = "Success, Reddedilerek Yonlendirildi.";
            this.success = 1;
            // this.modalService.dismissAll();
            this.getList();
          } else {
            this.message = "Failed";
            this.success = 2;
          }

          this.prossesing = false;
        },
        err => {
          console.log(err);
          this.prossesing = false;

        }
      );
    }
    else if (i == 3) {
      this.request = new RequestData();
      this.request.requestType = RequestType.Create;
      this.actionDetail.actionId = this.item.id;
      this.actionDetail.action = null;
      this.actionDetail.actionAssign = null;
      this.actionAssign = new ActionAssign();
      if (this.newactionAssign.assignUserId > 0)
        this.actionAssign.assignUserId = this.newactionAssign.assignUserId;
      else this.actionAssign.assignUserId = this.item.createdUserId;
      this.actionAssign.actionId = this.item.id;
      this.actionAssign.status = 1;
      if (this.actionDetail.name != "")
        this.actionAssign.actionDetails.push(this.actionDetail);
      this.actionAssign.action = null;
      this.request.data = this.actionAssign;
      this.service.ActionAssignCrud(this.request).subscribe(
        res => {
          console.log(res, "actionassign Res ");
          this.responseCreateDetail = <ResponseData>res;
          if (this.responseCreateDetail.isSucceed) {
            this.message = "Success, Action Kapatildi ve sorumlu ekranina kapatildi olarak dustu.";
            this.success = 1;

            // this.modalService.dismissAll();
            this.getList();
          } else {
            this.message = "Failed";
            this.success = 2;
          }

          this.prossesing = false;
        },
        err => {
          console.log(err);
          this.prossesing = false;

        }
      );
    }

    // console.log(this.actionAssign,"actionAssign");
  };


}


