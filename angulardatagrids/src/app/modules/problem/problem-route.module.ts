import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from 'src/app/layouts';
import { FooterModule } from 'src/app/shared/components';
import { DxDataGridModule, DxTreeViewModule, DxPopupModule, DxScrollViewModule, DxAccordionModule } from 'devextreme-angular';
import { NewProblemComponent } from "./new-problem/new-problem.component";
import { FormsModule } from '@angular/forms';
import { ProblemService } from './service';
import { AppInfoDialogComponent } from './componentsOfProblem/app-info-dialog-component/app-info-dialog.component';
import { AppInfoComponent } from './componentsOfProblem/app-info-component/app-info.component';
import { FormatsDialogComponent } from './componentsOfProblem/formats-dialog-component/formats-dialog.component';
import { ProblemUiModule } from './scannerFiles/problem-ui.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {WebcamModule} from 'ngx-webcam';
import { ProblemListComponent } from './problem-list/problem-list.component';
import { LightboxModule } from 'ngx-lightbox';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
const routes: Routes = [
  {
    path: '',
    component: NewProblemComponent
  },
  {
    path: 'list',
    component: ProblemListComponent
  }
];

@NgModule({
  declarations: [NewProblemComponent,FormatsDialogComponent, AppInfoComponent, AppInfoDialogComponent, ProblemListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    DxDataGridModule,
    FormsModule,
    ProblemUiModule,
    CKEditorModule,
    WebcamModule,
    LightboxModule,
    DxTreeViewModule,
    FontAwesomeModule,NgbModule,
    DxPopupModule,
    DxScrollViewModule,
    DxAccordionModule
  ],
  entryComponents: [FormatsDialogComponent, AppInfoDialogComponent],
  providers:[ProblemService],
  exports: [RouterModule]
})
export class ProblemRouteModule { }
