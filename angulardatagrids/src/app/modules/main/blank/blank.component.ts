import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-blank',
  templateUrl: './blank.component.html',
  styleUrls: ['./blank.component.scss']
})
export class BlankComponent implements OnInit {

  constructor(public router: Router, public cookie: CookieService) {
    if (this.cookie.get("refresh") === "1")
      {this.router.navigate(['/main/graph'])}
      else {
        this.cookie.set("refresh","1");
        window.location.reload();
      }
  }


  ngOnInit() {
  }

}
