import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart
} from "ng-apexcharts";
import { MainService } from '../service/main.service';
import { RequestData, ResponseData, FilterObject, RequestType } from 'src/app/model';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  title: string = "Hosgeldiniz.";
  public chartOptions: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  @ViewChild("chart", { static: false }) chart: ChartComponent;
  isRefresh: boolean;
  request: RequestData;
  responseData: ResponseData;
  filter: FilterObject = new FilterObject();
  constructor(private mainService:MainService) {
    this.getPie();
    this.chartOptions = {
      series: [],
      chart: {
        width: '100%',
        type: "pie"
      },
      labels: []
    };
    this.chartOptions2 = {
      series: [],
      chart: {
        width: '100%',
        type: "pie"
      },
      labels: []
    };
  }

  ngOnInit() {

  }
  public getPie() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column = "actionCategoryId";
    this.filter.condition = "==";
    this.filter.value = "2";
    this.request.request.filters.push(this.filter);
    this.mainService.GetActionStatisticForPieChart(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        console.log(this.responseData);

        this.chartOptions.series=this.responseData[0].series;
        this.chartOptions.labels[0]="Kapali";
        this.chartOptions.labels[1]="Acik";
        this.chartOptions2.series=this.responseData[1].series;
        this.chartOptions2.labels[0]="Kapali";
        this.chartOptions2.labels[1]="Acik";
      },
      err => {
        console.log(err);
      }
    );
  }
}
