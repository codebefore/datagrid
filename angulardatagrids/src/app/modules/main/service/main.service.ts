import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { RequestData } from 'src/app/model';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  // url:string='http://localhost:5001/';
  url: string = "https://prometal.azurewebsites.net/";

  //url Parameters
  private pieUrlParameter: string = "GetActionStatistic";
  constructor(private http: HttpClient, private cookie: CookieService) {}

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.cookie.get("token"),
      "Access-Control-Allow-Origin": "*"
    });
    return headers;
  }

  GetActionStatisticForPieChart(request: RequestData) {
    return this.http.post(this.url + this.pieUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

}
