import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { MainService } from './service/main.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LightboxModule } from 'ngx-lightbox';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SingleCardModule, SideNavOuterToolbarModule } from 'src/app/layouts';
import { FooterModule } from 'src/app/shared/components';
import { NgApexchartsModule } from "ng-apexcharts";
import { BlankComponent } from './blank/blank.component';

const routes: Routes = [
  {
    path: '',
    component: BlankComponent
  },
  {
    path: 'graph',
    component: MainComponent
  }
];

@NgModule({
  declarations: [MainComponent, BlankComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SideNavOuterToolbarModule,
    SingleCardModule,
    FooterModule,
    FormsModule,
    NgbModule,
    LightboxModule,
    FontAwesomeModule,
    NgApexchartsModule
  ],
  providers:[MainService],
  exports: [RouterModule]
})
export class MainRouteModule { }
