import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { RouterModule, Routes } from '@angular/router';
import { DxButtonModule, DxCheckBoxModule, DxTextBoxModule, DxValidatorModule, DxValidationGroupModule } from 'devextreme-angular';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
 
  {
    path: '',
    component: LoginLayoutComponent
  }
];

@NgModule({
  declarations: [LoginLayoutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RouterModule,
    DxButtonModule,
    DxCheckBoxModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxValidationGroupModule,
    FormsModule
  ],
  exports: [RouterModule]
})
export class LoginModule { }
