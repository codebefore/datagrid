import { Component, OnInit } from '@angular/core';

import { AuthService, AppInfoService } from '../../../shared/services';
import { DefinitionService } from '../../definition/service/definition.service';
import { RequestData, Login } from 'src/app/model';
import { LoginResponse } from 'src/app/model/loginResponse';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.scss']
})
export class LoginLayoutComponent implements OnInit {
  public login: Login = new Login();
  public isClick: boolean = false;
  public responseData: LoginResponse = new LoginResponse();
  public roleIds: string[] = new Array<string>();
  constructor(public authService: AuthService,
    public defService: DefinitionService,
    public appInfo: AppInfoService,
    public cookie: CookieService,
    private router: Router) { }

  onLoginClick = (args) => {
    console.log(args);

    this.isClick = true;
    // if (!args.validationGroup.validate().isValid) {
    //   this.isClick=false;
    //   return;
    // }
    const request = new RequestData();
    request.data = this.login;
    this.defService.token(request).subscribe(
      res => {
        this.responseData = <LoginResponse>res;
        console.log(this.responseData);

        if (this.responseData.isSucceed)
          this.cookie.set('token', this.responseData.accessToken);
        this.cookie.set('name', this.responseData.user.fullName);
        this.cookie.set('user', JSON.stringify(this.responseData.user));

        for (let index = 0; index < this.responseData.user.userRoles.length; index++) {
          const roleId = this.responseData.user.userRoles[index].roleId;
          this.roleIds.push(roleId.toString());
        }


        this.cookie.set('roles', JSON.stringify(this.roleIds));
        console.log(JSON.stringify(this.roleIds), 'roleIds');

        this.authService.logIn(this.login.email, this.login.password);
        
        // args.validationGroup.reset();
        this.isClick = false;
      },
      err => {
        this.isClick = false;
        console.log(err, 'Login err');
      }
    );
  }
  ngOnInit() { }
}
