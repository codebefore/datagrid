import { Component, OnInit } from '@angular/core';
import { Department, Factory, User, DefinitionService, DeviceGroup } from '../service';
import { ResponseData, RequestData, FilterObject, RequestType } from 'src/app/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-device-group',
  templateUrl: './device-group.component.html',
  styleUrls: ['./device-group.component.scss']
})
export class DeviceGroupComponent implements OnInit {
  dataSource: any = {};
  departments: Department[];
  factorys: Factory[];
  items: DeviceGroup[];
  users: User[];
  item:Department= new Department();
  responseData: ResponseData;
  responseUsers: ResponseData;
  request: RequestData;
  filter:FilterObject= new FilterObject();
  title:string='Cihaz Gruplari';
  constructor(public service: DefinitionService, public http: HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    this.getList();
    this.getUsers();
    this.getCategories();
  }
  ngOnInit() {}
  insert(e) {
    this.request.requestType = RequestType.Create;
    this.item = <DeviceGroup>e.data;
    this.item.typeId=4;
    this.request.data = e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e) {
    this.request.requestType = RequestType.Update;
    this.request.data = <DeviceGroup>e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "udpate ok");
        else console.log(this.responseData, "udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType = RequestType.Delete;
    this.request.data = <DeviceGroup>e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "delete ok");
        else console.log(this.responseData, "delete failed");
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if (!gridCell) {
      return;
    }
  }

  public getList() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column="typeId";
    this.filter.condition="==";
    this.filter.value="4";
    this.request.request.filters.push(this.filter);
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.items = this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );

  }

  public getUsers() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.service.UserCrud(this.request).subscribe(
      res => {
        console.log(res,"users ")
        this.responseUsers = <ResponseData>res;
        if (this.responseUsers.isSucceed) 
        this.users = this.responseUsers.data;
      },
      err => {
        console.log(err);
      }
    );

  }

  

  public getCategories() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column="typeId";
    this.filter.condition="==";
    this.filter.value="7";
    this.request.request.filters.push(this.filter);
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.departments = this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );

  }

  

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
}
