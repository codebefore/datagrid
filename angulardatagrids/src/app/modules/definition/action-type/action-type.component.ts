import { Component, OnInit } from '@angular/core';
import { ActionType } from '../service/action-type';
import { ResponseData, RequestData, RequestType } from 'src/app/model';
import { DefinitionService } from '../service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-action-type',
  templateUrl: './action-type.component.html',
  styleUrls: ['./action-type.component.scss']
})
export class ActionTypeComponent implements OnInit {

  dataSource: any={};
  actionTypes: ActionType[];
  responseData: ResponseData;
  request:RequestData=new RequestData();
  title:string='Uygunsuzluk Tipleri';
  constructor(public service: DefinitionService,
    public http:HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    this.getList();
  }
  ngOnInit(){
    
  }
  insert(e) {
    this.request.requestType=RequestType.Create;
    this.request.data=e.data;
    this.service.ActionTypeCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e) {
    this.request.requestType=RequestType.Update;
    this.request.data=e.data;
    this.service.ActionTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"udpate ok");
        else 
        console.log(this.responseData,"udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType=RequestType.Delete;
    this.request.data=e.data;
    this.service.ActionTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"delete ok");
        else 
        console.log(this.responseData,"delete failed");
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if(!gridCell) {
        return;
    }
}

public getList(){
  this.request.requestType=RequestType.List;
    this.service.ActionTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        this.actionTypes=this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );
  }
  
  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
}

