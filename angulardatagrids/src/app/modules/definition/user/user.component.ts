import { Component, OnInit, enableProdMode } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { User } from "../service/user";
import {
  ResponseData,
  RequestData,
  RequestType,
  FilterObject
} from "src/app/model";
import { DefinitionService } from "../service/definition.service";
import { UserType, Role } from "../service";

// if (!/localhost/.test(document.location.host)) {
//   enableProdMode();
// }

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  dataSource: any = {};
  users: User[];
  user: User = new User();
  types: UserType[];
  roles: Role[];
  responseData: ResponseData;
  request: RequestData;
  filter: FilterObject = new FilterObject();
  role: Role = new Role();
  title: string = 'Kullanicilar';

  constructor(public service: DefinitionService, public http: HttpClient) {
    this.getRoles();

    this.getTypes();
  }
  ngOnInit() { }
  onEditorPreparing(e: any) {
    if (
      (e.parentType === "dataRow" || e.parentType === "filterRow") &&
      e.dataField === "roles"
    ) {
      console.log("dxDropDownBox", this.roles);

      e.editorName = "dxTagBox";
      e.editorOptions.dataSource = this.roles;
      e.editorOptions.showSelectionControls = false;
      e.editorOptions.showDropButton= true,
      e.editorOptions.displayExpr = "name";
      e.editorOptions.valueExpr = "id";
      e.editorOptions.value = e.value || [];
      e.editorOptions.onValueChanged =
        function (args:any) {
          e.setValue(args.value);
        };
    }
  }
  insert(e: { data: any; }) {
    console.log(e, "insert");

    this.request = new RequestData();
    this.request.requestType = RequestType.Create;
    this.request.data = e.data;
    this.service.UserCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e: { data: User; }) {
    this.request.requestType = RequestType.Update;

    this.user = e.data;
    for (let i = 0; i < this.user.roles.length; i++) {
      for (let index = 0; index < this.user.userRoles.length; index++) {
        if (this.user.roles[i] == this.user.userRoles[index].roleId)
          this.user.roles.splice(i, 1);
      }
    }
    this.user.userRoles = null;
    this.request.data = this.user;
    console.log(e.data, "updateRequest");

    this.service.UserCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "udpate ok");
        else console.log(this.responseData, "udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e: { data: any; }) {
    this.request.requestType = RequestType.Delete;
    this.request.data = e.data;
    this.service.UserCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "delete ok");
        else console.log(this.responseData, "delete failed");
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options: { gridCell: any; }) {
    var gridCell = options.gridCell;
    if (!gridCell) {
      return;
    }
  }

  public getList() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.request.request.includes.push("UserRoles.Role");

    this.service.UserCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) {
          this.users = this.responseData.data;
          for (var i = 0; i < this.users.length; i++) {
            const userRoles = this.users[i].userRoles;
            this.users[i].roles = new Array<number>();
            for (let index = 0; index < userRoles.length; index++) {
              const userRole = userRoles[index];
              // this.role.id=userRole.roleId;
              // this.role.name=userRole.role.name;

              this.users[i].roles.push(userRole.roleId);
            }
          }
          console.log(this.users, "users");
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  public getTypes() {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.service.UserTypeCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        console.log(this.responseData, "userTypes");
        if (this.responseData.isSucceed)
          this.types = this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );
  }

  public getRoles = () => {
    this.request = new RequestData();
    this.request.requestType = RequestType.List;
    this.service.RoleCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        console.log(this.responseData, "roles");
        if (this.responseData.isSucceed)
          this.roles = this.responseData.data;
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
  public getStateNames = (user: any) => {
    let names: string[] = [];
    if (user.roles != null) {
      for (var i = 0; i < user.roles.length; i++) {
        let role = this.roles.find(x => x.id == user.roles[i]);
        if (role.name)
          names.push(role.name);
      }
    }

    return names.join(", ");
  };
}
