import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefinitionService } from './service/definition.service';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from 'src/app/layouts';
import { FooterModule } from 'src/app/shared/components';
import { DxDataGridModule, DxTagBoxModule } from 'devextreme-angular';
import { RouterModule, Routes } from '@angular/router';
import { RoleComponent, UserComponent, ActionCategoryComponent, UserTypeComponent,AssetTypeComponent,FactoryComponent,DepartmentComponent } from '.';
import { DeviceGroupComponent } from './device-group/device-group.component';
import { DeviceComponent } from './device/device.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ActionTypeComponent } from './action-type/action-type.component';
import { AtolyeComponent } from './atolye/atolye.component';
import { KisimComponent } from './kisim/kisim.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent
  },
  {
    path: 'role',
    component: RoleComponent
  }
  ,
  {
    path: 'action-category',
    component: ActionCategoryComponent
  }

  ,
  {
    path: 'action-type',
    component: ActionTypeComponent
  }
  ,
  {
    path: 'user-type',
    component: UserTypeComponent
  },
  {
    path: 'asset-type',
    component: AssetTypeComponent
  },
  {
    path: 'factory',
    component: FactoryComponent
  },
  {
    path: 'department',
    component: DepartmentComponent
  }
  ,
  {
    path: 'atolye',
    component: AtolyeComponent
  },
  {
    path: 'kisim',
    component: KisimComponent
  },
  {
    path: 'device-group',
    component: DeviceGroupComponent
  }
  ,
  {
    path: 'device',
    component: DeviceComponent
  }
  
];

@NgModule({
  declarations: [UserComponent, RoleComponent, ActionCategoryComponent, UserTypeComponent, AssetTypeComponent, FactoryComponent, DepartmentComponent, DeviceGroupComponent, DeviceComponent, ActionTypeComponent, AtolyeComponent, KisimComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    DxDataGridModule,
    NgxQRCodeModule,
    DxTagBoxModule
  ],
  providers:[DefinitionService],
  exports: [RouterModule]
})
export class DefinitionRouteModule { }
