import { Component, OnInit } from '@angular/core';
import { Department, User, DefinitionService, Factory,Atolye } from '../service';
import { ResponseData, RequestData, RequestType, FilterObject } from 'src/app/model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-atolye',
  templateUrl: './atolye.component.html',
  styleUrls: ['./atolye.component.scss']
})
export class AtolyeComponent implements OnInit {
  dataSource: any = {};
  itemlist: Atolye[];
  categories: Department[];
  users: User[];
  item:Atolye= new Atolye();
  responseData: ResponseData;
  responseUsers: ResponseData;
  request: RequestData;
  filter:FilterObject= new FilterObject();
  title:string='Atolyeler';
  constructor(public service: DefinitionService, public http: HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    this.getList();
    this.getUsers();
    this.getCategories();
  }
  ngOnInit() {}
  insert(e) {
    this.request.requestType = RequestType.Create;
    this.item = <Atolye>e.data;
    this.item.typeId=6;
    this.request.data = e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e) {
    this.request.requestType = RequestType.Update;
    this.request.data = <Atolye>e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "udpate ok");
        else console.log(this.responseData, "udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType = RequestType.Delete;
    this.request.data = <Atolye>e.data;
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed)
          console.log(this.responseData, "delete ok");
        else console.log(this.responseData, "delete failed");
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if (!gridCell) {
      return;
    }
  }

  public getList() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column="typeId";
    this.filter.condition="==";
    this.filter.value="6";
    this.request.request.filters.push(this.filter);
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) 
        {this.itemlist = this.responseData.data;
         
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  public getUsers() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.service.UserCrud(this.request).subscribe(
      res => {
        console.log(res,"users ")
        this.responseUsers = <ResponseData>res;
        if (this.responseUsers.isSucceed) 
        this.users = this.responseUsers.data;
      },
      err => {
        console.log(err);
      }
    );

  }

  public getCategories() {
    this.request= new RequestData();
    this.request.requestType = RequestType.List;
    this.filter.column="typeId";
    this.filter.condition="==";
    this.filter.value="3";
    this.request.request.filters.push(this.filter);
    this.service.AssetCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if (this.responseData.isSucceed) this.categories = this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );

  }

  

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
}