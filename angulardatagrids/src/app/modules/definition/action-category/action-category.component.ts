import { Component, OnInit } from '@angular/core';
import { ActionCategory } from '../service/action-category';
import { ResponseData, RequestData, RequestType } from 'src/app/model';
import { DefinitionService } from '../service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-action-category',
  templateUrl: './action-category.component.html',
  styleUrls: ['./action-category.component.scss']
})
export class ActionCategoryComponent implements OnInit {

  dataSource: any={};
  actionCategories: ActionCategory[];
  responseData: ResponseData;
  request:RequestData=new RequestData();
  title:string='Uygunsuzluk Kategory';
  constructor(public service: DefinitionService,
    public http:HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    this.getList();
  }
  ngOnInit(){
    
  }
  insert(e) {
    this.request.requestType=RequestType.Create;
    this.request.data=e.data;
    this.service.ActionCategoryCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"insert ok");
        else 
        console.log(this.responseData,"insert failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  update(e) {
    this.request.requestType=RequestType.Update;
    this.request.data=e.data;
    this.service.ActionCategoryCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"udpate ok");
        else 
        console.log(this.responseData,"udpate failed");
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType=RequestType.Delete;
    this.request.data=e.data;
    this.service.ActionCategoryCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if(!gridCell) {
        return;
    }
}

public getList(){
  this.request.requestType=RequestType.List;
    this.service.ActionCategoryCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        this.actionCategories=this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );
  }
  
  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
}

