import { Component, OnInit, Input } from '@angular/core';
import { ResponseData, RequestData, RequestType } from 'src/app/model';
import { DefinitionService,Role,User } from '../service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  dataSource: any={};
  roles: Role[];
  responseData: ResponseData;
  request:RequestData=new RequestData();
  value : string = '123456789';
  title:string='Roller';
  @Input('qrc-margin') margin = 4;
  @Input('qrc-scale') scale = 4;
  @Input('qrc-width') width = 10;
  
  constructor(public service: DefinitionService,
    public http:HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }
    this.getList();
  }
  ngOnInit(){
    
  }
  insert(e) {
    this.request.requestType=RequestType.Create;
    this.request.data=e.data;
    this.service.RoleCrud(this.request).subscribe(
      res => {
        this.getList();
      },
      err => {
        console.log(err);
      }
     
    );
  }
  update(e) {
    this.request.requestType=RequestType.Update;
    this.request.data=e.data;
    this.service.RoleCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"udpate ok");
        else 
        console.log(this.responseData,"udpate failed");
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }
  delete(e) {
    this.request.requestType=RequestType.Delete;
    this.request.data=e.data;
    this.service.RoleCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        console.log(this.responseData,"delete ok");
        else 
        console.log(this.responseData,"delete failed");
        this.getList();
      },
      err => {
        console.log(err);
      }
    );
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if(!gridCell) {
        return;
    }
}



public getList(){
  this.request.requestType=RequestType.List;
    this.service.RoleCrud(this.request).subscribe(
      res => {
        this.responseData = <ResponseData>res;
        if(this.responseData.isSucceed)
        this.roles=this.responseData.data;
      },
      err => {
        console.log(err);
      }
    );
  }
  
  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }
}
