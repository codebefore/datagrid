export class Item {
    id:number;
    parentId:number;
    text:string="";
    expanded:boolean;
    items:Item[];
}
