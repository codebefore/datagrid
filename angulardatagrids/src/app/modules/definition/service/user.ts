import { UserRole } from './user-role';

export class User {
    id:number=0;
    name:string;
    lastName:string;
    fullName:string;
    sicilNo:string;
    password:string;
    userRoles:UserRole[]=new Array<UserRole>();
    roles:number[];
    phone:string;
}
