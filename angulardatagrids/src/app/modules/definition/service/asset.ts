import { AssetType, User } from '.';

export class Asset {
    id:number;
    parentId:number;
    typeId:number;
    name:string="";
    assetType:AssetType;
    resUser:User;
    resUserId:number;
    isRecursive:boolean=false;
}
