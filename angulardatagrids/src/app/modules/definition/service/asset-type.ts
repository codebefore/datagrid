import { Asset } from '.';

export class AssetType {
    id:number=0;
    name:string;
    assets:Asset[];
}

