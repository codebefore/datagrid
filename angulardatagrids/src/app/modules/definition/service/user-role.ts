import { Role } from './role';

export class UserRole {
    userId:number=0;
    roleId:number;
    role:Role;
}
