import { Injectable } from "@angular/core";
import { ResponseData, RequestData } from "src/app/model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: "root"
})
export class DefinitionService {
  // url:string='http://localhost:5001/';
  url: string = "https://prometal.azurewebsites.net/";

  //url Parameters
  private userUrlParameter: string = "api/users";
  private roleUrlParameter: string = "api/roles";
  private actionCategoryUrlParameter: string = "api/actionCategorys";
  private actionTypeUrlParameter: string = "api/actionTypes";
  private userTypeUrlParameter: string = "api/userTypes";
  private assetTypeUrlParameter: string = "api/assetTypes";
  private assetUrlParameter: string = "api/assets";
  private pieUrlParameter: string = "GetActionStatistic";
  constructor(private http: HttpClient, private cookie: CookieService) {}

  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.cookie.get("token"),
      "Access-Control-Allow-Origin": "*"
    });
    return headers;
  }



  token(request: RequestData) {
    return this.http.post(this.url + "token", request.data, {
      headers: this.createRequestOptions()
    });
  }

  UserCrud(request: RequestData) {
    return this.http.post(this.url + this.userUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  RoleCrud(request: RequestData) {
    return this.http.post(this.url + this.roleUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  ActionCategoryCrud(request: RequestData) {
    return this.http.post(this.url + this.actionCategoryUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  ActionTypeCrud(request: RequestData) {
    return this.http.post(this.url + this.actionTypeUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  UserTypeCrud(request: RequestData) {
    return this.http.post(this.url + this.userTypeUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  AssetTypeCrud(request: RequestData) {
    return this.http.post(this.url + this.assetTypeUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  AssetCrud(request: RequestData) {
    return this.http.post(this.url + this.assetUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

  GetActionStatisticForPieChart(request: RequestData) {
    return this.http.post(this.url + this.pieUrlParameter, request, {
      headers: this.createRequestOptions()
    });
  }

}
