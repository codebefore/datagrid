import { Component, OnInit, enableProdMode } from "@angular/core";
import { AppService } from "src/app/app.service";
import { City, ResponseData } from "src/app/model";
import { HttpClient, HttpHeaders } from '@angular/common/http';

if (!/localhost/.test(document.location.host)) {
  enableProdMode();
}

@Component({
  selector: "app-tablegrid",
  templateUrl: "./tablegrid.component.html",
  styleUrls: ["./tablegrid.component.scss"]
})
export class TablegridComponent implements OnInit {
  dataSource: any={};
  cities: City[];
  responseData: ResponseData;
  // cityRequset: CityRequest = new CityRequest();

  constructor(public service: AppService,
    public http:HttpClient) {
    // function isNotEmpty(value: any): boolean {
    //   return value !== undefined && value !== null && value !== "";
    // }

    // this.getList();   
  }

  

  insert(e) {
    console.log(e,'inserted');
  }
  update(e) {
    console.log(e,'update');
  }
  delete(e) {
    console.log(e,'delete');
  }

  customizeExcelCell(options) {
    var gridCell = options.gridCell;
    if(!gridCell) {
        return;
    }
}

  ngOnInit() {}
  
  public createRequestOptions() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    return headers;
  }

  // public getList(){
  //   this.service.getCities(this.cityRequset).subscribe(
  //     res => {
  //       this.responseData = <ResponseData>res;
  //       console.log(this.responseData,'şehirler listesi ok');
  //       if(this.responseData.isSucceed)
  //       this.cities=this.responseData.data;
  //     },
  //     err => {
  //       console.log(err,'şehirler listesi err');
  //     }
  //   );
  // }
}
